# 4x4x4 LED Cube

## 4x4x4 Cube Editor (Windows)

### Setupordner
[/4x4x4_Cube_Editor/4x4x4CUBE_EDITOR/4x4x4CUBE_EDITOR/publish/](/4x4x4_Cube_Editor/4x4x4CUBE_EDITOR/4x4x4CUBE_EDITOR/publish/ "Setup Ordner")

### Kurzberschreibung

* Software zum leichteren Erstellen der Binär Pattern
* LED werden durch Klick auf diese umgeschaltet
* Das aktuelle LED Abbild wird dann in das Datatable mit NEW übernommen
* Durch Doppelklick auf die Zeile Im DataTable kann mann das Abbild erneut aufrufen, bearbeiten und entweder als neue Zeile speichern oder als Änderung übernehmen.
* Über speichern werden die Kunstwerke als *.xml gespeichert und können wieder geladen werden.
* Export erzeugt einen Text mit den Binärcode welcher dann in das [Vorlage_4x4x4_gemeinsameAnode.ino](/4x4x4_Cube_Editor/Vorlage_4x4x4_gemeinsameAnode/Vorlage_4x4x4_gemeinsameAnode.ino "Vorlage_4x4x4_gemeinsameAnode.ino") oder [Vorlage_4x4x4_gemeinsameKathode.ino](/4x4x4_Cube_Editor/Vorlage_4x4x4_gemeinsameKathode/Vorlage_4x4x4_gemeinsameKathode.ino "Vorlage_4x4x4_gemeinsameKathode.ino") kopiert werden kann.


![MAIN](/4x4x4_Cube_Editor/main.PNG  "Hauptprogramm")
 
![Export](/4x4x4_Cube_Editor/Export.PNG  "ExprotBildschirm")