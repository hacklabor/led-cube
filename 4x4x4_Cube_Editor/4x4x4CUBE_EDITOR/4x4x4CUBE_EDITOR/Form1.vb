﻿Public Class Form1
    Dim LED(3, 3, 3) As Boolean
    Dim CLIK_POS_X(3, 3, 3) As Integer
    Dim CLIK_POS_y(3, 3, 3) As Integer
    Dim nur_einmal, simmustart As Boolean
    Dim dgvRow As DataGridViewRow
    Dim anzahlDS As Integer
    Dim SimuZähler As Integer
    Dim Timer As Integer
    Dim graph As Graphics
    Dim image As New Bitmap(450, 450)

    Private Sub Label81_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Panel1_Paint(sender As Object, e As PaintEventArgs) Handles Panel1.Paint
        graph = e.Graphics
        Dim höhe As Integer = sender.Size.Height
        graph.DrawImage(image, 0, 0)
        Dim i As Byte = 3

        'Ebene(graph, Color.Green, i * 25, i * 20, 100, höhe, True)
        'For w = 0 To 3
        '    Würfel(graph, Color.Black, 0, 100 * w, 100, höhe)
        'Next



        Kreise(graph, i * 25, i * 20, 100, höhe, i)

        i = i - 1
        ' Ebene(graph, Color.Red, i * 25, i * 20, 100, höhe, True)
        Kreise(graph, i * 25, i * 20, 100, höhe, i)
        i = i - 1
        'Ebene(graph, Color.Blue, i * 25, i * 20, 100, höhe, True)
        Kreise(graph, i * 25, i * 20, 100, höhe, i)
        i = i - 1
        ' Ebene(graph, Color.Black, i * 25, i * 20, 100, höhe, True)

        Kreise(graph, i * 25, i * 20, 100, höhe, i)
        If nur_einmal = False Then

        End If
        nur_einmal = True


    End Sub

    Private Sub Ebene(ByRef graph1 As Graphics, farbe As Color, offset_x As Integer, offset_y As Integer, Abstand As Integer, höhe As Integer, voll As Boolean)

        Dim stifft As New Pen(farbe, 2)

        Dim x_off As Integer = 40
        Dim y_off As Integer = 45

        x_off += offset_x
        y_off += offset_y
        y_off = höhe - y_off

        For s = 0 To 3
            graph1.DrawLine(stifft, x_off + 0, y_off - (s * Abstand), x_off + Abstand * 3, y_off - (s * Abstand))
        Next
        If voll = True Then
            For s = 0 To 3
                graph1.DrawLine(stifft, x_off + (s * Abstand), y_off, x_off + (s * Abstand), y_off - Abstand * 3)
            Next
        End If

    End Sub
    Private Sub Würfel(ByRef graph1 As Graphics, farbe As Color, offset_x As Integer, offset_y As Integer, Abstand As Integer, höhe As Integer)
        Dim stifft As New Pen(farbe, 2)

        Dim x_off As Integer = 40
        Dim y_off As Integer = 45
        x_off += offset_x
        y_off += offset_y

        y_off = höhe - y_off

        For s = 0 To 3
            graph1.DrawLine(stifft, x_off + s * Abstand, y_off, x_off + 3 * 25 + s * Abstand, y_off - (3 * 20))
        Next


    End Sub
    Private Sub Kreise(ByRef graph1 As Graphics, offset_x As Integer, offset_y As Integer, Abstand As Integer, höhe As Integer, EBENE As Byte)
        Dim stifft As New SolidBrush(Color.Blue)
        Dim st As New Pen(Color.Black, 2)

        Dim x_off As Integer = 40
        Dim y_off As Integer = 45

        x_off += offset_x
        y_off += offset_y

        y_off = höhe - y_off
        For s = 0 To 3
            For z = 0 To 3
                If LED(z, EBENE, s) = True Then
                    stifft.Color = Color.Blue

                Else
                    stifft.Color = Color.White

                End If
                graph1.FillEllipse(stifft, x_off - 10 + (z * Abstand), y_off - 10 - (s * Abstand), 20, 20)
                graph1.DrawEllipse(st, x_off - 10 + (z * Abstand), y_off - 10 - (s * Abstand), 20, 20)
                If nur_einmal = False Then
                    CLIK_POS_X(z, EBENE, s) = x_off + (z * Abstand)
                    CLIK_POS_y(z, EBENE, s) = (y_off - (s * Abstand))
                End If
            Next
        Next


    End Sub

    Private Sub Panel1_MouseClick(sender As Object, e As MouseEventArgs) Handles Panel1.MouseClick
        Dim mouse As Point
        Dim data As String = "B"
        Dim zaehler As Byte = 0
        mouse = Panel1.PointToClient(MousePosition)

        Dim pos_X As Integer = mouse.X



        Dim pos_y As Integer = mouse.Y


        For z = 0 To 3
            For y = 0 To 3
                For x = 0 To 3
                    If zaehler = 4 Then
                        Data += ",B"
                        zaehler = 0
                    End If
                    zaehler += 1
                    If (mouse.X > CLIK_POS_X(x, y, z) - 15) And (mouse.X < CLIK_POS_X(x, y, z) + 15) Then
                        If (mouse.Y > CLIK_POS_y(x, y, z) - 15) And (mouse.Y < CLIK_POS_y(x, y, z) + 15) Then
                            If LED(x, y, z) = True Then
                                LED(x, y, z) = False
                            Else
                                LED(x, y, z) = True
                            End If
                            TextBox3.Text = x.ToString + ", " + y.ToString + ", " + z.ToString
                            Panel1.Invalidate()
                        End If

                    End If
                    If LED(x, y, z) = True Then

                        data += "1"
                    Else

                        data += "0"
                    End If
                Next
            Next
        Next
        TextBox3.Text = data
    End Sub

    Private Sub alle_EIN_AUS(ein As Boolean)
        Dim data As String = "B"
        Dim zaehler As Byte = 0
        For z = 0 To 3
            For y = 0 To 3
                For x = 0 To 3
                    If zaehler = 4 Then
                        Data += ",B"
                        zaehler = 0
                    End If
                    zaehler += 1
                    If ein = True Then
                        LED(x, y, z) = 1
                        Data += "1"
                    Else

                        LED(x, y, z) = 0
                        Data += "0"
                    End If
                Next
            Next
        Next
        TextBox3.Text = data
    End Sub

    Private Sub Bt_AlleEIN_Click(sender As Object, e As EventArgs) Handles Bt_AlleEIN.Click
        alle_EIN_AUS(True)
        Panel1.Refresh()
    End Sub

    Private Sub Bt_AlleAUS_Click(sender As Object, e As EventArgs) Handles Bt_AlleAUS.Click
        alle_EIN_AUS(False)
        Panel1.Refresh()
    End Sub

    Private Sub Bt_New_Click(sender As Object, e As EventArgs) Handles Bt_New.Click
        Dim row As DataRow = Pattern.Tables(0).NewRow
        Pattern.Tables(0).Rows.Add(row)
        row(0) = TextBox3.Text
        row(1) = nud_Time.Value

    End Sub



    Private Sub dgv1_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv1.CellDoubleClick
        dgvRow = dgv1.CurrentRow
        nud_Time.Value = dgvRow.Cells(1).Value
        Dim dat() As String = dgvRow.Cells(0).Value.ToString.Split(",")

        For y = 0 To 3
            For z = 0 To 3
                For x = 0 To 3
                    LED(x, y, z) = dat(y + z * 4).Substring((x + 1), 1)
                Next
            Next

        Next
        Panel1.Refresh()
        Bt_Change.Enabled = True
    End Sub

    Private Sub Bt_Change_Click(sender As Object, e As EventArgs) Handles Bt_Change.Click
        dgvRow.Cells(1).Value = nud_Time.Value
        dgvRow.Cells(0).Value = TextBox3.Text

    End Sub

    Private Sub Bt_Simmulation_Click(sender As Object, e As EventArgs) Handles Bt_Simmulation.Click
        simmustart = True
        SimuZähler = 0
        Timer = 10000000
        Timer1.Start()
        anzahlDS = dgv1.RowCount - 1
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Timer1.Stop()
        simmustart = False
    End Sub



    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Timer1.Stop()

        Timer += 1
        If Timer >= CInt(nud_Time.Value / 10) Then
            dgvRow = dgv1.Rows(SimuZähler)
            If SimuZähler = 0 Then
                dgv1.Rows(anzahlDS - 1).Selected = False
            Else
                dgv1.Rows(SimuZähler - 1).Selected = False
            End If
            dgv1.Rows(SimuZähler).Selected = True
            nud_Time.Value = dgvRow.Cells(1).Value
            Dim dat() As String = dgvRow.Cells(0).Value.ToString.Split(",")

            For y = 0 To 3
                For z = 0 To 3
                    For x = 0 To 3
                        LED(x, y, z) = dat(y + z * 4).Substring((x + 1), 1)
                    Next
                Next

            Next
            Panel1.Refresh()
            Timer = 0
            SimuZähler += 1
            If anzahlDS = SimuZähler Then SimuZähler = 0
        End If

        Timer1.Start()
    End Sub

    Private Sub Bt_Speichern_Click(sender As Object, e As EventArgs) Handles Bt_Speichern.Click
        If SaveFileDialog1.ShowDialog = DialogResult.OK Then
            Pattern.WriteXml(SaveFileDialog1.FileName)
        End If
    End Sub

    Private Sub Bt_laden_Click(sender As Object, e As EventArgs) Handles Bt_laden.Click
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            Pattern.ReadXml(OpenFileDialog1.FileName)
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim txt As String = ""
        For i = 0 To dgv1.RowCount - 2
            dgvRow = dgv1.Rows(i)
            txt += dgvRow.Cells(0).Value + ", " + CInt(dgvRow.Cells(1).Value / 10).ToString + ", " + vbCrLf
        Next
        Ausgabe.tb1.Text = txt
        Ausgabe.ShowDialog()


    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        graph = Graphics.FromImage(image)
        Dim höhe As Integer = Panel1.Height



        Dim i As Byte = 3

        Ebene(graph, Color.Green, i * 25, i * 20, 100, höhe, True)
        For w = 0 To 3
            Würfel(graph, Color.Black, 0, 100 * w, 100, höhe)
        Next



        Kreise(graph, i * 25, i * 20, 100, höhe, i)

        i = i - 1
        Ebene(graph, Color.Red, i * 25, i * 20, 100, höhe, True)
        Kreise(graph, i * 25, i * 20, 100, höhe, i)
        i = i - 1
        Ebene(graph, Color.Blue, i * 25, i * 20, 100, höhe, True)
        Kreise(graph, i * 25, i * 20, 100, höhe, i)
        i = i - 1
        Ebene(graph, Color.Black, i * 25, i * 20, 100, höhe, True)

        Kreise(graph, i * 25, i * 20, 100, höhe, i)
        Try

            Me.Text += " Version: " + My.Application.Deployment.CurrentVersion.ToString
        Catch ex As Exception

        End Try


    End Sub


End Class
