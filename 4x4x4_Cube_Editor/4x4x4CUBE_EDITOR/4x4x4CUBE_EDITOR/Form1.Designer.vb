﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Bt_Change = New System.Windows.Forms.Button()
        Me.nud_Time = New System.Windows.Forms.NumericUpDown()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Bt_AlleEIN = New System.Windows.Forms.Button()
        Me.Bt_AlleAUS = New System.Windows.Forms.Button()
        Me.Pattern = New System.Data.DataSet()
        Me.DataTable1 = New System.Data.DataTable()
        Me.DataColumn1 = New System.Data.DataColumn()
        Me.DataColumn2 = New System.Data.DataColumn()
        Me.DataColumn3 = New System.Data.DataColumn()
        Me.dgv1 = New System.Windows.Forms.DataGridView()
        Me.LEDStringDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TimeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NrDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Bt_New = New System.Windows.Forms.Button()
        Me.Bt_Simmulation = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Bt_Speichern = New System.Windows.Forms.Button()
        Me.Bt_laden = New System.Windows.Forms.Button()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.SaveFileDialog1 = New System.Windows.Forms.SaveFileDialog()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.nud_Time, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Pattern, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.White
        Me.Panel1.Location = New System.Drawing.Point(2, 1)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(450, 450)
        Me.Panel1.TabIndex = 0
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(0, 457)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(600, 20)
        Me.TextBox3.TabIndex = 3
        '
        'Bt_Change
        '
        Me.Bt_Change.Enabled = False
        Me.Bt_Change.Location = New System.Drawing.Point(576, 279)
        Me.Bt_Change.Name = "Bt_Change"
        Me.Bt_Change.Size = New System.Drawing.Size(75, 79)
        Me.Bt_Change.TabIndex = 4
        Me.Bt_Change.Text = "DS Ändern"
        Me.Bt_Change.UseVisualStyleBackColor = True
        '
        'nud_Time
        '
        Me.nud_Time.Increment = New Decimal(New Integer() {100, 0, 0, 0})
        Me.nud_Time.Location = New System.Drawing.Point(576, 249)
        Me.nud_Time.Maximum = New Decimal(New Integer() {2550, 0, 0, 0})
        Me.nud_Time.Minimum = New Decimal(New Integer() {10, 0, 0, 0})
        Me.nud_Time.Name = "nud_Time"
        Me.nud_Time.Size = New System.Drawing.Size(54, 20)
        Me.nud_Time.TabIndex = 5
        Me.nud_Time.Value = New Decimal(New Integer() {500, 0, 0, 0})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(631, 251)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(20, 13)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "ms"
        '
        'Bt_AlleEIN
        '
        Me.Bt_AlleEIN.Location = New System.Drawing.Point(483, 217)
        Me.Bt_AlleEIN.Name = "Bt_AlleEIN"
        Me.Bt_AlleEIN.Size = New System.Drawing.Size(75, 23)
        Me.Bt_AlleEIN.TabIndex = 7
        Me.Bt_AlleEIN.Text = "Alle EIN"
        Me.Bt_AlleEIN.UseVisualStyleBackColor = True
        '
        'Bt_AlleAUS
        '
        Me.Bt_AlleAUS.Location = New System.Drawing.Point(483, 246)
        Me.Bt_AlleAUS.Name = "Bt_AlleAUS"
        Me.Bt_AlleAUS.Size = New System.Drawing.Size(75, 23)
        Me.Bt_AlleAUS.TabIndex = 8
        Me.Bt_AlleAUS.Text = "Alle AUS"
        Me.Bt_AlleAUS.UseVisualStyleBackColor = True
        '
        'Pattern
        '
        Me.Pattern.DataSetName = "NewDataSet"
        Me.Pattern.Tables.AddRange(New System.Data.DataTable() {Me.DataTable1})
        '
        'DataTable1
        '
        Me.DataTable1.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn1, Me.DataColumn2, Me.DataColumn3})
        Me.DataTable1.TableName = "Table1"
        '
        'DataColumn1
        '
        Me.DataColumn1.Caption = "LED String"
        Me.DataColumn1.ColumnName = "LED String"
        '
        'DataColumn2
        '
        Me.DataColumn2.ColumnName = "Time"
        '
        'DataColumn3
        '
        Me.DataColumn3.AutoIncrement = True
        Me.DataColumn3.ColumnName = "Nr"
        Me.DataColumn3.DataType = GetType(Integer)
        '
        'dgv1
        '
        Me.dgv1.AutoGenerateColumns = False
        Me.dgv1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.LEDStringDataGridViewTextBoxColumn, Me.TimeDataGridViewTextBoxColumn, Me.NrDataGridViewTextBoxColumn})
        Me.dgv1.DataMember = "Table1"
        Me.dgv1.DataSource = Me.Pattern
        Me.dgv1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgv1.Location = New System.Drawing.Point(0, 483)
        Me.dgv1.Name = "dgv1"
        Me.dgv1.Size = New System.Drawing.Size(684, 178)
        Me.dgv1.TabIndex = 9
        '
        'LEDStringDataGridViewTextBoxColumn
        '
        Me.LEDStringDataGridViewTextBoxColumn.DataPropertyName = "LED String"
        Me.LEDStringDataGridViewTextBoxColumn.HeaderText = "LED String"
        Me.LEDStringDataGridViewTextBoxColumn.Name = "LEDStringDataGridViewTextBoxColumn"
        Me.LEDStringDataGridViewTextBoxColumn.Width = 83
        '
        'TimeDataGridViewTextBoxColumn
        '
        Me.TimeDataGridViewTextBoxColumn.DataPropertyName = "Time"
        Me.TimeDataGridViewTextBoxColumn.HeaderText = "Time"
        Me.TimeDataGridViewTextBoxColumn.Name = "TimeDataGridViewTextBoxColumn"
        Me.TimeDataGridViewTextBoxColumn.Width = 55
        '
        'NrDataGridViewTextBoxColumn
        '
        Me.NrDataGridViewTextBoxColumn.DataPropertyName = "Nr"
        Me.NrDataGridViewTextBoxColumn.HeaderText = "Nr"
        Me.NrDataGridViewTextBoxColumn.Name = "NrDataGridViewTextBoxColumn"
        Me.NrDataGridViewTextBoxColumn.Width = 43
        '
        'Bt_New
        '
        Me.Bt_New.Location = New System.Drawing.Point(483, 279)
        Me.Bt_New.Name = "Bt_New"
        Me.Bt_New.Size = New System.Drawing.Size(75, 79)
        Me.Bt_New.TabIndex = 10
        Me.Bt_New.Text = "DS New"
        Me.Bt_New.UseVisualStyleBackColor = True
        '
        'Bt_Simmulation
        '
        Me.Bt_Simmulation.Location = New System.Drawing.Point(604, 400)
        Me.Bt_Simmulation.Name = "Bt_Simmulation"
        Me.Bt_Simmulation.Size = New System.Drawing.Size(80, 37)
        Me.Bt_Simmulation.TabIndex = 11
        Me.Bt_Simmulation.Text = "Simmulation Start"
        Me.Bt_Simmulation.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        Me.Timer1.Interval = 10
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(604, 443)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(80, 34)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Simmulation Stop"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(588, 233)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(31, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "ZEIT"
        '
        'Bt_Speichern
        '
        Me.Bt_Speichern.Location = New System.Drawing.Point(526, 399)
        Me.Bt_Speichern.Name = "Bt_Speichern"
        Me.Bt_Speichern.Size = New System.Drawing.Size(75, 23)
        Me.Bt_Speichern.TabIndex = 14
        Me.Bt_Speichern.Text = "speichern"
        Me.Bt_Speichern.UseVisualStyleBackColor = True
        '
        'Bt_laden
        '
        Me.Bt_laden.Location = New System.Drawing.Point(526, 428)
        Me.Bt_laden.Name = "Bt_laden"
        Me.Bt_laden.Size = New System.Drawing.Size(75, 23)
        Me.Bt_laden.TabIndex = 15
        Me.Bt_laden.Text = "laden"
        Me.Bt_laden.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        Me.OpenFileDialog1.Filter = "Xml-Datein|*.xml"
        '
        'SaveFileDialog1
        '
        Me.SaveFileDialog1.Filter = "Xml-Datein|*.xml"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(458, 428)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(62, 23)
        Me.Button2.TabIndex = 16
        Me.Button2.Text = "export"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(684, 661)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Bt_laden)
        Me.Controls.Add(Me.Bt_Speichern)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Bt_Simmulation)
        Me.Controls.Add(Me.dgv1)
        Me.Controls.Add(Me.Bt_New)
        Me.Controls.Add(Me.Bt_AlleAUS)
        Me.Controls.Add(Me.Bt_AlleEIN)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.nud_Time)
        Me.Controls.Add(Me.Bt_Change)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.Text = "4x4x4 Cube Pattern Editor By ThK"
        CType(Me.nud_Time, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Pattern, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataTable1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents Bt_Change As Button
    Friend WithEvents nud_Time As NumericUpDown
    Friend WithEvents Label1 As Label
    Friend WithEvents Bt_AlleEIN As Button
    Friend WithEvents Bt_AlleAUS As Button
    Friend WithEvents Pattern As DataSet
    Friend WithEvents DataTable1 As DataTable
    Friend WithEvents DataColumn1 As DataColumn
    Friend WithEvents DataColumn2 As DataColumn
    Friend WithEvents DataColumn3 As DataColumn
    Friend WithEvents dgv1 As DataGridView
    Friend WithEvents Bt_New As Button
    Friend WithEvents LEDStringDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TimeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents NrDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Bt_Simmulation As Button
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Button1 As Button
    Friend WithEvents Label2 As Label
    Friend WithEvents Bt_Speichern As Button
    Friend WithEvents Bt_laden As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents SaveFileDialog1 As SaveFileDialog
    Friend WithEvents Button2 As Button
End Class
